package at.mobilrace.maturaprojekt;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.view.View.OnClickListener;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.regist);
		if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
          }
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiLock wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL , "MyWifiLock");
        wifiLock.acquire();

        Button register = (Button) findViewById(R.id.btnOK);
        
        register.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            	EditText username = (EditText) findViewById(R.id.edUsername);
                EditText password = (EditText) findViewById(R.id.edPassword);
                EditText email = (EditText) findViewById(R.id.edemail);
                EditText verein = (EditText) findViewById(R.id.edverein);
                EditText telefonnr = (EditText) findViewById(R.id.edtelefon);
                

                Register(username, password, email, telefonnr, verein);
            }
        });

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.regist, menu);
		return true;
	}
	 protected void Register(EditText username, EditText password, EditText email, EditText telefonnr, EditText verein)
	    {           
	    	// Create a new HttpClient and Post Header
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost("http://www.mobilrace.com/android-php/regist.php");

	        try {
	            // Add your data
	            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	            nameValuePairs.add(new BasicNameValuePair("name", username.getText().toString()));
	            nameValuePairs.add(new BasicNameValuePair("pass", password.getText().toString()));
	            nameValuePairs.add(new BasicNameValuePair("email",email.getText().toString()));
	            nameValuePairs.add(new BasicNameValuePair("telefonnr",telefonnr.getText().toString()));
	            nameValuePairs.add(new BasicNameValuePair("verein",verein.getText().toString()));
	            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	            // Execute HTTP Post Request
	            HttpResponse response = httpclient.execute(httppost);
	            int status = response.getStatusLine().getStatusCode();
	            Toast.makeText(getApplicationContext(),"Danke f�r die Registrierung!",
	                      Toast.LENGTH_LONG).show();

	            
	        } catch (ClientProtocolException e) {
	        	Toast.makeText(getApplicationContext(), e.toString(),
	                      Toast.LENGTH_SHORT).show();
	            
	        } catch (IOException e) {
	        	Toast.makeText(getApplicationContext(), e.toString(),
	                      Toast.LENGTH_SHORT).show();
	        }
	    }
	 public static String sha256(String base) {
		    try{
		        MessageDigest digest = MessageDigest.getInstance("SHA-256");
		        byte[] hash = digest.digest(base.getBytes("UTF-8"));
		        StringBuffer hexString = new StringBuffer();

		        for (int i = 0; i < hash.length; i++) {
		            String hex = Integer.toHexString(0xff & hash[i]);
		            if(hex.length() == 1) hexString.append('0');
		            hexString.append(hex);
		        }

		        return hexString.toString();
		    } catch(Exception ex){
		       throw new RuntimeException(ex);
		    }
		}

}
