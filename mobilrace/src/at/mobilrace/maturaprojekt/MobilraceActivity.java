package at.mobilrace.maturaprojekt;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class MobilraceActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mobilrace);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mobilrace, menu);
		return true;
	}

}
