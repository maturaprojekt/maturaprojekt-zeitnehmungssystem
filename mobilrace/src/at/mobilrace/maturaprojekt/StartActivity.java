package at.mobilrace.maturaprojekt;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
        
        Button register = (Button)findViewById(R.id.btnRegister);
        Button login = (Button)findViewById(R.id.btnLogin);
        Button shalltracks = (Button)findViewById(R.id.btnTracks);
        Button edUser = (Button)findViewById(R.id.btnedituser1);
        Button deluser = (Button)findViewById(R.id.btndeleteuser);
        Button deltrack = (Button)findViewById(R.id.btndeletetrack);
        login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	//Aufruf LoginActivity
            	Login();
            }
        });
        
        register.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	//Aufruf RegistActivity
            	Regist();
            }
        });
        
        shalltracks.setOnClickListener(new OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		shTracks();
        	}
        });
        edUser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	//Aufruf LoginActivity
            	editUser();
            }
        });
        deluser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	//Aufruf Activity
            	delUser();
            }
        });
        deltrack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	//Aufruf Activity
            	delTrack();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }
    
    public void Login()
    {
    	Intent i = new Intent(this,LoginActivity.class);
    	startActivity(i);
    }
    public void Regist()
    {
    	Intent i = new Intent(this,RegistActivity.class);
    	startActivity(i);
    }
    public void shTracks()
    {
    	Intent i = new Intent(this, GetAllTracks.class);
    	startActivity(i);
    }
    public void editUser()
    {
    	Intent i = new Intent(this, EditUser.class);
    	startActivity(i);
    }
    public void delUser()
    {
    	Intent i = new Intent(this, DeleteUser.class);
    	startActivity(i);
    }
    public void delTrack()
    {
    	Intent i = new Intent(this, DeleteTrack.class);
    	startActivity(i);
    }
    
    
}
