package at.mobilrace.maturaprojekt;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DeleteUser extends Activity {
	
	JSONParser jsonParser = new JSONParser();
	private static final String TAG_SUCCESS = "success";
	private static final String url_delUser = "http://www.mobilrace.com/android-php/delete_user.php";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.delete_user);
		if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
          }
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiLock wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL , "MyWifiLock");
        wifiLock.acquire();

        Button delUser = (Button) findViewById(R.id.btnDeleteUser);
        
        delUser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            	EditText id = (EditText) findViewById(R.id.eddelID);
            	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("id", id.getText().toString()));
                
                JSONObject json = jsonParser.makeHttpRequest(url_delUser,
    					"POST", nameValuePairs);
                try {
    				int success = json.getInt(TAG_SUCCESS);

    				if (success == 1) {
    					// successfully edited
    					Toast.makeText(getApplicationContext(), json.toString(), Toast.LENGTH_SHORT).show();
    				}
    				else {
    					
    					Log.d("note:", json.toString());
    				}
    			} catch (JSONException e) {
    				Log.d("json exception!" , e.toString());
    			}
                

                
            }
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.delete_user, menu);
		return true;
	}

}
